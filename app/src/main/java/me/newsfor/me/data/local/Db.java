package me.newsfor.me.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import me.newsfor.me.data.model.News;

/**
 * Created by Alex Karpenko on 16.06.17.
 */

public class Db {

    public Db() {
    }

    public abstract static class NewsTable {
        public static final String TABLE_NAME = "my_news";

        public static final String COLUMN_ID = "id_news";
        public static final String COLUMN_TOP = "top_news";
        public static final String COLUMN_TYPE = "type_news";
        public static final String COLUMN_CONTENT = "news_content";
        public static final String COLUMN_HEX_COLOR = "hex_color";


        public static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_ID + " TEXT PRIMARY KEY, " +
                        COLUMN_TOP + " TEXT NOT NULL, " +
                        COLUMN_TYPE + " TEXT NOT NULL, " +
                        COLUMN_CONTENT + " BLOB NOT NULL, " +
                        COLUMN_HEX_COLOR + " TEXT NOT NULL " +
                        " ); ";

        /*
        *  update news
        * */

        public static ContentValues toContentValues(News news) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, news.id);
            values.put(COLUMN_TOP, news.top);
            values.put(COLUMN_TYPE, news.type);
            values.put(COLUMN_CONTENT, news.content);
            values.put(COLUMN_HEX_COLOR, news.hexColor);
            return values;
        }


        /*
        * Request to news
        * */
        public static News parseNews(Cursor cursor) {
            News news = new News();
            news.setId(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ID)));
            news.setTop(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TOP)));
            news.setType(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TYPE)));
            news.setContent(cursor.getBlob(cursor.getColumnIndexOrThrow(COLUMN_CONTENT)));
            news.setHexColor(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_HEX_COLOR)));
            return news;
        }
    }
}
