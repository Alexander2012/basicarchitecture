package me.newsfor.me.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public class News {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("top")
    @Expose
    public String top;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("content")
    @Expose
    public byte[] content;

    @SerializedName("hexColor")
    @Expose
    public String hexColor;

    public void setId(String id) {
        this.id = id;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }
}
