package me.newsfor.me.data.remote;

import java.util.List;

import me.newsfor.me.data.model.News;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public interface NewsService {

    String ENDPOINT = "https://api.news.com/";

    @GET("news")
    Observable<List<News>> getNews();


    class Creator {
        public static NewsService newsService() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(NewsService.ENDPOINT)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(NewsService.class);
        }
    }
}
