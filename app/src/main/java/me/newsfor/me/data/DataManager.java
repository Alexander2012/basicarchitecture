package me.newsfor.me.data;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import me.newsfor.me.data.local.DatabaseHelper;
import me.newsfor.me.data.local.PreferencesHelper;
import me.newsfor.me.data.model.News;
import me.newsfor.me.data.remote.NewsService;
import me.newsfor.me.util.Utils;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Alex Karpenko on 16.06.17.
 */

@Singleton
public class DataManager {

    private final NewsService mNewsService;
    private final DatabaseHelper mDatabaseHelper;
    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public DataManager(NewsService newsService, PreferencesHelper preferencesHelper,
                       DatabaseHelper databaseHelper) {
        mNewsService = newsService;
        mPreferencesHelper = preferencesHelper;
        mDatabaseHelper = databaseHelper;
    }

    /*
    * Settings for prefernces
    * */

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    /**
     * If i have api i will sync next fuction
     */

//    public Observable<News> syncNews() {
//        return mNewsService.getListNews()
//                .concatMap(new Func1<List<News>, Observable<News>>() {
//                    @Override
//                    public Observable<News> call(List<News> news) {
//                        return mDatabaseHelper.setNews(news);
//                    }
//                });
//    }

    /*
    * Sync with server
    * */

    public Observable<News> syncNews() {
        final ArrayList<News> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            News news = new News();
            String num = String.valueOf(i);
            news.setId(num);
            news.setType("Political " + num);
            news.setContent(Utils.byteArrayText());
            news.setTop("Top " + num);
            news.setHexColor(Utils.randomColor());
            list.add(news);
        }
        return Observable.just(list)
                .concatMap(new Func1<List<News>, Observable<News>>() {
                    @Override
                    public Observable<News> call(List<News> news) {
                        return mDatabaseHelper.setNews(list);
                    }
                });
    }

    /*
    * Get list of news
    * */

    public Observable<List<News>> getListNews() {
        return mDatabaseHelper.getNews().distinct();
    }

    /*
    * Request for one news
    * */
    public Observable<News> getNews(String id) {
        return mDatabaseHelper.getNewsForPage(id);
    }

}
