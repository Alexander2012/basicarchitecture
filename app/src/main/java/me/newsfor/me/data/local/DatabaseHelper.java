package me.newsfor.me.data.local;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import me.newsfor.me.data.model.News;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Alex Karpenko on 16.06.17.
 */


@Singleton
public class DatabaseHelper {

    private final BriteDatabase mDb;

    @Inject
    public DatabaseHelper(DbOpenHelper dbOpenHelper) {
        SqlBrite.Builder briteBuilder = new SqlBrite.Builder();
        mDb = briteBuilder.build().wrapDatabaseHelper(dbOpenHelper, Schedulers.immediate());
    }

    public BriteDatabase getBriteDb() {
        return mDb;
    }

    public Observable<News> setNews(final Collection<News> myNews) {
        return Observable.create(new Observable.OnSubscribe<News>() {
            @Override
            public void call(Subscriber<? super News> subscriber) {
                if (subscriber.isUnsubscribed()) return;
                BriteDatabase.Transaction transaction = mDb.newTransaction();
                try {
                    mDb.delete(Db.NewsTable.TABLE_NAME, null);
                    for (News news : myNews) {
                        long result = mDb.insert(Db.NewsTable.TABLE_NAME,
                                Db.NewsTable.toContentValues(news),
                                SQLiteDatabase.CONFLICT_REPLACE);
                        if (result >= 0) subscriber.onNext(news);
                    }
                    transaction.markSuccessful();
                    subscriber.onCompleted();
                } finally {
                    transaction.end();
                }
            }
        });
    }

    public Observable<List<News>> getNews() {
        return mDb.createQuery(Db.NewsTable.TABLE_NAME,
                "SELECT * FROM " + Db.NewsTable.TABLE_NAME)
                .mapToList(new Func1<Cursor, News>() {
                    @Override
                    public News call(Cursor cursor) {
                        return Db.NewsTable.parseNews(cursor);
                    }
                });
    }

    public Observable<News> getNewsForPage(String id) {
        return mDb.createQuery(Db.NewsTable.TABLE_NAME,
                "SELECT * FROM " + Db.NewsTable.TABLE_NAME + " WHERE " + Db.NewsTable.COLUMN_ID + " = " + id)
                .mapToOne(new Func1<Cursor, News>() {
                    @Override
                    public News call(Cursor cursor) {
                        return Db.NewsTable.parseNews(cursor);
                    }
                });
    }
}
