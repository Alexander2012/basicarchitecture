package me.newsfor.me.ui.main;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.newsfor.me.R;
import me.newsfor.me.data.model.News;
import me.newsfor.me.util.Utils;
import me.newsfor.me.util.interfaces.OnItemClick;

/**
 * Created by Alex Karpenko on 16.06.17.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private List<News> mNews;
    private OnItemClick mClick;

    @Inject
    public NewsAdapter() {
        this.mNews = new ArrayList<>();
    }



    public void setNews(List<News> news,OnItemClick click) {
        this.mNews = news;
        this.mClick = click;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);
        return new NewsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsViewHolder holder, int position) {
        News news = mNews.get(position);
        holder.hexColorView.setBackgroundColor(Color.parseColor(news.hexColor));
        holder.top.setText(news.top);

        holder.type.setText(Utils.convertToString(news.content));

    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_hex_color) View hexColorView;
        @BindView(R.id.top) TextView top;
        @BindView(R.id.type) TextView type;
        @BindView(R.id.card_view) CardView cardView;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.card_view)
        public void onClick() {

            mClick.itemClick(mNews.get(getAdapterPosition()).id);
        }

    }

}
