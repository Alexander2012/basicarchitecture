package me.newsfor.me.ui.base;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public class BasePresenter<T extends MvpView> implements Presenter<T> {

    private CompositeSubscription mCompositeSubscription;

    private T mMvpView;

    @Override
    public void attachView(T mvpView) {
        mCompositeSubscription = new CompositeSubscription();
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
        mCompositeSubscription.unsubscribe();
    }

    public void addSubscription(Subscription subscription) {
        mCompositeSubscription.add(subscription);
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public T getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }
}

