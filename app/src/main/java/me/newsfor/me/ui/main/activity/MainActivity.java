package me.newsfor.me.ui.main.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.newsfor.me.R;
import me.newsfor.me.data.SyncService;
import me.newsfor.me.data.model.News;
import me.newsfor.me.ui.base.BaseActivity;
import me.newsfor.me.ui.main.presenter.MainPresenter;
import me.newsfor.me.ui.main.NewsAdapter;
import me.newsfor.me.ui.mvp.MainMvpView;
import me.newsfor.me.util.interfaces.OnItemClick;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */


public class MainActivity extends BaseActivity implements MainMvpView, OnItemClick {

    private static final String SYNC_FLAG =
            "SYNC_FLAG";

    @Inject MainPresenter mMainPresenter;
    @Inject NewsAdapter mNewsAdapter;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mRecyclerView.setAdapter(mNewsAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMainPresenter.attachView(this);
        mMainPresenter.loadListNews();

        if (getIntent().getBooleanExtra(SYNC_FLAG, true)) {
            startService(SyncService.getStartIntent(this));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mMainPresenter.detachView();
    }

    /***** Update list news *****/

    @Override
    public void showNews(List<News> news) {
        mNewsAdapter.setNews(news, this);
        mNewsAdapter.notifyDataSetChanged();
    }

    @Override
    public void showNewsEmpty() {
        mNewsAdapter.setNews(Collections.<News>emptyList(), this);
        mNewsAdapter.notifyDataSetChanged();
        Toast.makeText(this, R.string.empty_news, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorInternet() {
        Toast.makeText(this,R.string.error, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void itemClick(String id) {
        startActivity(new Intent(this, DetailsNewsActivity.class).putExtra("ID",id));
    }
}
