package me.newsfor.me.ui.mvp;

import java.util.List;

import me.newsfor.me.data.model.News;
import me.newsfor.me.ui.base.MvpView;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */


public interface MainMvpView extends MvpView {

    void showNews(List<News> news);

    void showNewsEmpty();


}
