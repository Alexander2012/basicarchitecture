package me.newsfor.me.ui.base;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;

import me.newsfor.me.NewsApplication;
import me.newsfor.me.injection.component.ActivityComponent;
import me.newsfor.me.injection.component.DaggerActivityComponent;
import me.newsfor.me.injection.module.ActivityModule;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

@SuppressLint("Registered")

public abstract class BaseActivity extends AppCompatActivity {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    String activityControllerExceptionMessage =
            String.format("You forgot to inject ActivityController inside %s.onCreate()\n" +
                            "or wrong implementation of %s.getActivityController()", CLASS_NAME, CLASS_NAME);

    private ActivityComponent mActivityComponent;

    public ActivityComponent getActivityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(NewsApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }


}
