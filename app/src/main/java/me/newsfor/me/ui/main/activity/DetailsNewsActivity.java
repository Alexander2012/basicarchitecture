package me.newsfor.me.ui.main.activity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.newsfor.me.R;
import me.newsfor.me.data.SyncService;
import me.newsfor.me.data.model.News;
import me.newsfor.me.ui.base.BaseActivity;
import me.newsfor.me.ui.main.presenter.DetailsNewsPresenter;
import me.newsfor.me.ui.mvp.DetailsNewsMvpView;
import me.newsfor.me.util.Utils;

/**
 * Created by Alex on 16.06.17.
 */

public class DetailsNewsActivity extends BaseActivity implements DetailsNewsMvpView {

    private static final String SYNC_FLAG =
            "SYNC_FLAG";

    @Inject DetailsNewsPresenter mDetailsNewsPresenter;
    @BindView(R.id.top) TextView mTop;
    @BindView(R.id.type) TextView mType;
    @BindView(R.id.content) TextView mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        mDetailsNewsPresenter.attachView(this);
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            String id = extras.getString("ID");
            mDetailsNewsPresenter.loadNew(id);
        }


        if (getIntent().getBooleanExtra(SYNC_FLAG, true)) {
            startService(SyncService.getStartIntent(this));
        }
    }

    /***** Update news *****/

    @Override
    public void showNews(News news) {
        mTop.setText(news.top);
        mType.setText(news.type);
        mContent.setText(Utils.convertToString(news.content));
    }

    @Override
    public void showNewsEmpty() {
        Toast.makeText(this, R.string.empty_news, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDetailsNewsPresenter.detachView();
    }

    @Override
    public void showErrorInternet() {

    }
}
