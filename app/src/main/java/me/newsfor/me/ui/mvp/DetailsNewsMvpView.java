package me.newsfor.me.ui.mvp;

import me.newsfor.me.data.model.News;
import me.newsfor.me.ui.base.MvpView;

/**
 * Created by Alex on 16.06.17.
 */

public interface DetailsNewsMvpView extends MvpView {

    void showNews(News news);

    void showNewsEmpty();


}
