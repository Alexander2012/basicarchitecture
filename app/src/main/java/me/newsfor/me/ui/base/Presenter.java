package me.newsfor.me.ui.base;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}
