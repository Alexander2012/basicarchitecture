package me.newsfor.me.ui.main.presenter;

import java.util.List;

import javax.inject.Inject;

import me.newsfor.me.data.DataManager;
import me.newsfor.me.data.model.News;
import me.newsfor.me.ui.base.BasePresenter;
import me.newsfor.me.ui.mvp.MainMvpView;
import me.newsfor.me.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public class MainPresenter extends BasePresenter<MainMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public MainPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void loadListNews() {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.getListNews()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<News>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().showErrorInternet();
                    }

                    @Override
                    public void onNext(List<News> news) {
                        if (news.isEmpty()) {
                            getMvpView().showNewsEmpty();
                        } else {
                            getMvpView().showNews(news);
                        }
                    }
                });
    }

}
