package me.newsfor.me.ui.base;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public interface MvpViewProgress extends MvpView {

    void showProgress();

    void hideProgress();
}
