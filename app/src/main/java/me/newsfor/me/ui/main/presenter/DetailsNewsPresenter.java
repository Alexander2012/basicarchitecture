package me.newsfor.me.ui.main.presenter;

import javax.inject.Inject;

import me.newsfor.me.data.DataManager;
import me.newsfor.me.data.model.News;
import me.newsfor.me.ui.base.BasePresenter;
import me.newsfor.me.ui.mvp.DetailsNewsMvpView;
import me.newsfor.me.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Alex on 16.06.17.
 */

public class DetailsNewsPresenter extends BasePresenter<DetailsNewsMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public DetailsNewsPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(DetailsNewsMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void loadNew(String id) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.getNews(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<News>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().showErrorInternet();
                    }

                    @Override
                    public void onNext(News news) {
                        if (news == null) {
                            getMvpView().showNewsEmpty();
                        } else {
                            getMvpView().showNews(news);
                        }
                    }
                });
    }

}
