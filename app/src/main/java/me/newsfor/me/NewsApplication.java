package me.newsfor.me;

import android.app.Application;
import android.content.Context;

import me.newsfor.me.injection.component.ApplicationComponent;
import me.newsfor.me.injection.component.DaggerApplicationComponent;
import me.newsfor.me.injection.module.ApplicationModule;
import timber.log.Timber;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public class NewsApplication extends Application {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    /*
    * Application context
    * */

    public static NewsApplication get(Context context) {
        return (NewsApplication) context.getApplicationContext();
    }

    /*
    * Dagger component
    * */
    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }

    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
