package me.newsfor.me.util;

import java.io.UnsupportedEncodingException;
import java.util.Random;

/**
 * Created by Alex on 16.06.17.
 */

public class Utils {

    public static String convertToString(byte [] news){
        String text = null;
        try {
            text = new String(news, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "Something wrong";
        }
        return text;
    }

    public static byte[] byteArrayText() {
        String s = " Content news about our life ";
        System.out.println(s);
        byte[] b = new byte[s.length()];
        for (int i = 0; i < b.length; i++) {
            int cp = s.codePointAt(i);
            b[i] = (byte) cp;
        }
        return b;
    }

    public static String randomColor() {
        return  String.format("#%06x", new Random().nextInt(256*256*256));
    }
}
