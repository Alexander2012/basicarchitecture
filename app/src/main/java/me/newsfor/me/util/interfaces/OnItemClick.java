package me.newsfor.me.util.interfaces;

/**
 * Created by Alex on 16.06.17.
 */

public interface OnItemClick {

    void itemClick(String id);
}
