package me.newsfor.me.util;

import rx.Subscription;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

public class RxUtil {

    public static void unsubscribe(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
