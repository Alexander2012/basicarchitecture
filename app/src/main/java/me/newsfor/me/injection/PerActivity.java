package me.newsfor.me.injection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
