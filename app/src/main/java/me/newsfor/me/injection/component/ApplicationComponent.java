package me.newsfor.me.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import me.newsfor.me.data.DataManager;
import me.newsfor.me.data.SyncService;
import me.newsfor.me.data.local.DatabaseHelper;
import me.newsfor.me.data.local.PreferencesHelper;
import me.newsfor.me.data.remote.NewsService;
import me.newsfor.me.injection.ApplicationContext;
import me.newsfor.me.injection.module.ApplicationModule;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);

    @ApplicationContext
    Context context();
    Application application();
    NewsService newsService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();

}
