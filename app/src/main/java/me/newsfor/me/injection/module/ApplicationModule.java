package me.newsfor.me.injection.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.newsfor.me.data.remote.NewsService;
import me.newsfor.me.injection.ApplicationContext;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    NewsService provideNewsService() {
        return NewsService.Creator.newsService();
    }

}
