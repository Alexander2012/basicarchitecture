package me.newsfor.me.injection.component;

import dagger.Component;
import me.newsfor.me.injection.PerActivity;
import me.newsfor.me.injection.module.ActivityModule;
import me.newsfor.me.ui.main.activity.DetailsNewsActivity;
import me.newsfor.me.ui.main.activity.MainActivity;

/**
 * Created by Alex Karpenko on 16.06.17.
 *
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(DetailsNewsActivity detailsNewsActivity);
}
